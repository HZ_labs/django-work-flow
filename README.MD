## HOW TO USE ##

You can use it no windows or linux.
(On your computer must have the following packages: Pip, Python v2.*, git)

    cd ~
    git clone https://bitbucket.org/pahaz/django-work-flow.git _DJANGO_PROJECTS_
    cd _DJANGO_PROJECTS_

Install `deploy` depensencies if need:

    pip install -r deploy/req.txt

Create `deploy/.pip.cache` dir and `deploy/fabsettings.py` file.

Example fabsettings.py:

    # conf for init
    BITBUCKET_USER = 'pahaz'
    BITBUCKET_PASSWORD = 'mypasswd'

    # conf for deploy
    DEPLOY_DEFAULT_DOMAIN = 'developers.urfu.ru'
    DEPLOY_DEFAULT_REPOSITORY = 'https://pahaz@bitbucket.org/pahaz/developers.urfu.ru.git'

    DEPLOY_USER = 'root'                            # change 
    DEPLOY_PASSWORD = 'qwer'                        # change
    DEPLOY_HOSTS = ['192.168.174.131:22', ]         # change

Run `init.sh.cmd` file for crete new Django site.

And enter you new project name.

    ~\_DJANGO_PROJS_$> init.sh.cmd
    [192.168.174.131:22] Executing task 'init'
    project domain: imkn.urfu.ru

Open new project directory in PyCharm (example) ~/_DJANGO_PROJECTS_/imkn.urfu.ru/src/

Run `deploy` if need deploy to remote server from git repository. (~/_DJANGO_PROJECTS_/imkn.urfu.ru/fabs.py)

### CONFIGURATION NEW DJANGO PROJECT ###

Edit `deploy/project` template (template for new Django projec - `src` dir).